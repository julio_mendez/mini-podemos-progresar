from flask import Flask, render_template
from api.podemos_api import api
import requests

app = Flask(__name__, template_folder='./templates', static_folder='./static', static_url_path='')
app.register_blueprint(api, url_prefix='/api')
base_url = 'http://localhost:7002/'


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/grupos')
def list_groups():
    groups = requests.get(base_url + 'api/grupos')
    return render_template('grupos/lista.html', groups=groups.json())


@app.route('/grupo/<group_id>')
def group_details(group_id):
    group = requests.get(base_url + 'api/grupos/' + group_id)
    return render_template('grupos/detalle.html', group=group.json())


@app.route('/cuenta/<account_id>')
def account_detail(account_id):
    account = requests.get(base_url + 'api/cuentas/' + account_id)
    return render_template('cuentas/detalle.html', account=account.json())


@app.route('/clientes')
def list_clients():
    clients =  requests.get(base_url + 'api/clientes')
    return render_template('clientes.html', clients=clients.json())


if __name__ == '__main__':
    # es importarte setear threaded a True para poder hacer peticiones a nuestra API
    # ya que las procesa el mismo server
    app.run(host='127.0.0.1', port=7002, debug=True, threaded=True)