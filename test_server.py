from server import app
import pytest

# Verifica que aquellos endpoints que retornan html sean alcanzados satisfactoriamente
def test_endpoints():
    with app.test_client() as tc:
        for endpoint in ['/', 'grupos', 'grupo/foo', 'clientes', 'cuenta/bar']:
            response = tc.get(endpoint)
            assert response.status_code == 200
            assert response.content_type == 'text/html; charset=utf-8'

@pytest.mark.parametrize('endpoint, methods, has_detail', [
    ('grupos', ['POST', 'GET'], True),
    ('clientes', ['GET'], False),
    ('transacciones', ['POST', 'GET'], False),
    ('cuentas', ['POST', 'GET'], True),
    ('calendario', ['POST', 'GET'], False)
])
def test_api_endpoints(endpoint, methods, has_detail):
    with app.test_client() as tc:
        for method in methods:
            if method == 'GET':
                url = '/api/' + endpoint + '/'
                url += 'foobar' if has_detail else ''
                response = tc.get(url)
            else:
                response = tc.post('/api/' + endpoint + '/')
        assert response.status_code == 200
        assert response.content_type == 'application/json'
