from flask import Blueprint

from .views.calendario import CalendarioView
from .views.clientes import ClienteView
from .views.cuentas import CuentasView
from .views.grupos import GruposView
from .views.serializer import CustomJsonSerializer
from .views.transacciones import TransaccionesView

api = Blueprint('api', __name__)
api.json_encoder = CustomJsonSerializer

api.add_url_rule('/clientes/', view_func=ClienteView.as_view('clientes'))

api.add_url_rule('/transacciones/', view_func=TransaccionesView.as_view('transacciones'), methods=['GET', 'POST'])

api.add_url_rule('/grupos/', view_func=GruposView.as_view('grupos'), defaults={'group_id': None})
api.add_url_rule('/grupos/<group_id>', view_func=GruposView.as_view('grupo'))

api.add_url_rule('/cuentas/', view_func=CuentasView.as_view('cuentas'), defaults={'account_id': None})
api.add_url_rule('/cuentas/<account_id>', view_func=CuentasView.as_view('cuenta'))

api.add_url_rule('/calendario/', view_func=CalendarioView.as_view('calendario'), methods=['GET', 'POST'])

