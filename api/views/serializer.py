from flask.json import JSONEncoder
from decimal import Decimal
from datetime import datetime
from datetime import date

class CustomJsonSerializer(JSONEncoder):
    def default(self, attr):
        if isinstance(attr, Decimal):
            return float(attr)
        if isinstance(attr, datetime):
            return attr.strftime('%Y-%m-%d')
        if isinstance(attr, date):
            return attr.strftime('%Y-%m-%d')
        return JSONEncoder.default(self, attr)


def fields_serializer(model_row):
    return {col.name: getattr(model_row, col.name) for col in model_row.__table__.columns}

def expand(model, expand_fields=[]):
    relationships = {}
    expand_fields = ['%s_collection' % r for r in expand_fields]

    for collection in [f for f in dir(model) if f in expand_fields]:
        collection_name = collection.split('_')[0]
        relationships[collection_name] = []
        for item in getattr(model, collection):
            relationships[collection_name].append(fields_serializer(item))

    return relationships

def serialize(model, expand_relations=[]):
    return {**expand(model, expand_relations), **fields_serializer(model)}
