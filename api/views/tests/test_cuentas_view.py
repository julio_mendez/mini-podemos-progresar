from server import app
import pytest

from api.views.cuentas import CuentasView
from api.models import NUM_PAGOS_X_PRESTAMOS, Clientes, Grupos
from api.views.serializer import CustomJsonSerializer
from decimal import Decimal
from mock import Mock

# Se debe configurar ya que las respuestas de la vista tienen datos del tipo Decimal y date
app.json_encoder = CustomJsonSerializer

@pytest.fixture
def group():
    client = Clientes()
    client.nombre = 'Pepo'
    client.id = '_pepo'

    group = Grupos()
    group.id = 'grpro'
    group.nombre = 'Atizapunk'

    client.grupos_collection.append(group)

    return group


def test_generate_calendar():

    loan_amount = '3500'

    with app.test_request_context(data={'generate_calendar': True, 'loan_amount': loan_amount}):
        view = CuentasView()
        response = view.post().json
        assert response['code'] == 200
        payments_data = response['data']

        assert payments_data['amount'] == (Decimal(loan_amount) / NUM_PAGOS_X_PRESTAMOS)
        assert len(payments_data['dates']) == NUM_PAGOS_X_PRESTAMOS


def test_create_calendar(group):
    data = {
        'group_id': 'grpro',
        'account_id': 'abxyz',
        'loan_amount': '8500'
    }
    with app.test_request_context(data=data):
        view = CuentasView()
        view._session.add(group)
        view._generate_calendar = Mock(return_value={'code': 200, 'message': 'Cuenta creada'})
        response = view.post()
        view._generate_calendar.assert_called_once()
        view._session.rollback()


def test_account_creation(group):

    with app.test_request_context():
        view = CuentasView()
        view._session.add(group)
        account_id = 'abxyz'
        amount = Decimal('8500')

        account = view._create_account(group, amount, account_id)

        assert account.grupos.id == group.id
        assert account.id == account_id
        assert account.estatus == 'DESEMBOLSADA'
        assert account.monto == amount
        assert account.saldo == amount

        view._session.rollback()


