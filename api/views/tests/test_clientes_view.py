from api.views.clientes import ClienteView
from mock import Mock
from server import app


def test_create_client():
    client_id = 'hegdb'
    client_name = 'Heimdal el guardian del bifrost'

    data = {
        'client_id': client_id,
        'client_name': client_name
    }

    with app.test_request_context(data=data):
        view = ClienteView()
        view._create_client = Mock()
        response = view.post().json

        view._create_client.assert_called_with(client_id, client_name)
        assert response['code'] == 200
