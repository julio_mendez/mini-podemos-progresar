import datetime
import json
from decimal import Decimal

import pytest
from flask import jsonify

from ..serializer import CustomJsonSerializer, fields_serializer, expand, serialize
from ...models import Clientes, Grupos, Cuentas


@pytest.fixture
def client():
    client = Clientes()
    client.nombre = 'Pepito'
    client.id = '_pepo'

    account = Cuentas()
    account.estatus = "CERRADA"
    account.id = "12345"
    account.monto = 75000
    account.saldo = 0

    group = Grupos()
    group.id = 'grpro'
    group.nombre = 'Los valedores de atizapan'
    group.cuentas_collection.append(account)

    client.grupos_collection.append(group)

    return client


def test_expand(client):
    expandable_fields = ['clientes', 'cuentas']
    expanded = expand(client.grupos_collection[0], expandable_fields)

    # No se esperan los campos del propio grupo ya que de eso se encarga la funcion serialize
    expected = {
        'clientes': [{'id': '_pepo', 'nombre': 'Pepito'}],
        'cuentas': [{'id': '12345', 'grupo_id': None, 'estatus': 'CERRADA', 'monto': 75000, 'saldo': 0}]
    }

    assert expanded == expected


def test_serialize(client):
    expandable_fields = ['clientes', 'cuentas']
    complete_object = serialize(client.grupos_collection[0], expandable_fields)

    expected = {
        'clientes': [{'id': '_pepo', 'nombre': 'Pepito'}],
        'cuentas': [{'id': '12345', 'grupo_id': None, 'estatus': 'CERRADA', 'monto': 75000, 'saldo': 0}],
        'id': 'grpro',
        'nombre': 'Los valedores de atizapan'
    }

    assert complete_object == expected


def test_field_serializer(client):
    object_serialized = fields_serializer(client)
    expected = {'id': '_pepo', 'nombre': 'Pepito'}
    assert expected == object_serialized


def test_custom_json():
    foo = {
        'key1': Decimal('10'),
        'key2': datetime.date(2020, 5, 20),
        'key3': datetime.datetime(2020, 5, 20)
    }

    with pytest.raises(TypeError):
        json.dumps(foo)

    expected = '{"key1": 10.0, "key2": "2020-05-20", "key3": "2020-05-20"}'
    assert json.dumps(foo, cls=CustomJsonSerializer) == expected
