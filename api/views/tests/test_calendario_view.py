
from datetime import date
from decimal import Decimal

import pytest
from api.models import CalendarioPagos, Grupos, Cuentas, session
from api.views.calendario import CalendarioView
from freezegun import freeze_time
from mock import Mock
from server import app


@pytest.fixture
def setup():
    group = Grupos(id='_yep_', nombre='Los testers')

    account = Cuentas(
        id='_cnta',
        estatus='DESEMBOLSADA',
        monto=Decimal('8000'),
        saldo=Decimal('8000'),
        grupos=group
    )

    to_pay = CalendarioPagos(
        id= 1000,
        cuentas=account,
        num_pago=1,
        monto=Decimal('2000'),
        fecha_pago=date(2020, 9, 16),
        estatus='PENDIENTE'
    )
    return account, group, to_pay


@freeze_time('2020-09-16')
@pytest.mark.parametrize('amount, expected_status', [
    (1000, 'PARCIAL'),
    (1000, 'ATRASADO'),
    (2000, 'PAGADO'),
])
def test_updating_payment_calendar_status(setup, amount, expected_status):

    account, group, to_pay = setup
    data = {'payment_id': to_pay.num_pago, 'payment_amount': amount, 'account_id': account.id}

    # este estatus depende de la fecha pactada del pago y la fecha en la que se realiza la transacción
    if expected_status == 'ATRASADO':
        to_pay.fecha_pago = date(2020, 9, 15)

    with app.test_request_context(data=data):
        view = CalendarioView()

        view._session.add(group)
        view._session.add(account)
        view._session.add(to_pay)
        view._create_transaction = Mock(return_value={'status': 200, 'message': 'created'})
        view._get_payment_calendar = Mock(return_value=to_pay)
        response = view.post()
        assert to_pay.estatus == expected_status
        view._create_transaction.assert_called()
        view._session.rollback()
