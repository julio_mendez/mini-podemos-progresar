from datetime import datetime
from decimal import Decimal, InvalidOperation

from flask import jsonify, request
from flask.views import MethodView

from ..models import Cuentas, Transacciones, session
from .mixins import ListViewMixin, GenerateTransactionsMixin
from .serializer import serialize


class TransaccionesView(MethodView, ListViewMixin, GenerateTransactionsMixin):
    _session = session
    model = Transacciones

    def get(self):
        return jsonify(self.list_view())


    def post(self):
        try:
            for key, field in [('account_id', 'Cuenta ID'), ('transaction_amount', 'Monto'), ('transaction_date', 'Fecha')]:
                assert request.form.get(key), '<%s>' % field
        except AssertionError as e:
            return jsonify({'code': 400, 'message': str(e)})

        account_id = request.form['account_id']
        deposit_account = self._session.query(Cuentas).get(account_id)

        if not deposit_account:
            return jsonify({'code': 400, 'message': 'No se encontró la cuenta con id %s' % account_id})

        try:
            transaction_amount = Decimal(request.form['transaction_amount'])
        except InvalidOperation:
            return jsonify({'code': 400, 'message': 'Por favor ingresa un monto correcto'})

        try:
            transaction_date = datetime.strptime(request.form['transaction_date'], '%Y-%m-%dT%H:%M')
        except ValueError:
            return jsonify({'code': 400, 'message': 'Formado de fecha y hora incorrecto'})

        return self._create_transaction(deposit_account, transaction_amount, transaction_date)
