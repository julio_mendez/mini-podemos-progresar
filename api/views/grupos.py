from flask.views import MethodView
from flask import jsonify
from .serializer import serialize
from ..models import Grupos, session
from .mixins import ListViewMixin, DetailViewMixin

class GruposView(MethodView, ListViewMixin, DetailViewMixin):
    _session = session
    model = Grupos
    expandable_fields = ['clientes', 'cuentas']

    def get(self, group_id):
        if group_id is None:
            return jsonify(self.list_view(expand_fields=['clientes']))

        group_detail = self._session.query(self.model).get(group_id)

        return jsonify(self.detail(group_detail) if group_detail else {})


