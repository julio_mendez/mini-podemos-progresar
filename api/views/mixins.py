from .serializer import serialize
from flask import jsonify
from ..models import Transacciones


class ListViewMixin():
    def list_view(self, expand_fields=[]):
        items = []
        for item in self._session.query(self.model).all():
            items.append(serialize(item, expand_fields))
        return items

class DetailViewMixin():
    def detail(self, instance, expand_fields=[]):
        return serialize(instance, expand_fields or self.expandable_fields)

class GenerateTransactionsMixin():

    def _close_payments_calendar(self, account):
        for pc in account.calendariopagos_collection:
            pc.estatus = 'PAGADO'
            self._session.add(pc)
            self._session.commit()

    def _create_transaction(self, account, tx_amount, tx_date):
        try:
            tx = Transacciones()
            tx.fecha = tx_date

            if tx_amount > account.saldo:
                return jsonify({'code': 400, 'message': 'El monto ingresado supera el saldo de la cuenta'})

            tx.monto = tx_amount
            tx.cuentas = account
            self._session.add(tx)
            self._session.commit()
            self._check_account_status(account, tx_amount)
            return jsonify({'code': 200, 'message': 'Transaccion creada exitosamente'})
        except Exception as e:
            return jsonify({'code': 500, 'message': 'Ocurrió un error al crear la transacción %s ' % str(e)})


    def _check_account_status(self, account, tx_amount):
        balance = account.saldo - tx_amount
        account.saldo = balance

        if balance == 0:
            account.estatus = 'CERRADA'
            self._close_payments_calendar(account)

        self._session.add(account)
        self._session.commit()
