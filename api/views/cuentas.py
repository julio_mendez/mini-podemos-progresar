import datetime
from decimal import Decimal, InvalidOperation

from flask import jsonify, request
from flask.views import MethodView

from ..models import (NUM_PAGOS_X_PRESTAMOS, CalendarioPagos, Cuentas, Grupos,
                      session)
from .mixins import DetailViewMixin, ListViewMixin
from .serializer import serialize


class CuentasView(MethodView, ListViewMixin, DetailViewMixin):
    _session = session
    model = Cuentas
    expandable_fields = ['transacciones', 'calendariopagos']

    def _get_calendar_dates(self, _loan_amount):
        loan_amount = Decimal(_loan_amount)
        today = datetime.datetime.today()
        calendar = {'dates': [], 'amount': loan_amount / NUM_PAGOS_X_PRESTAMOS}

        for i in range(1, NUM_PAGOS_X_PRESTAMOS + 1):
            calendar['dates'].append(today + datetime.timedelta(weeks=1 * i))

        return calendar

    def _generate_calendar(self, source_account, loan_amount):
        calendar = self._get_calendar_dates(loan_amount)
        partiality_amount = calendar['amount']

        for payment_num, date in enumerate(calendar['dates'], start=1):
            calendar_entry = CalendarioPagos()
            calendar_entry.cuentas = source_account
            calendar_entry.num_pago = payment_num
            calendar_entry.monto = partiality_amount
            calendar_entry.fecha_pago = date
            calendar_entry.estatus = 'PENDIENTE'
            self._session.add(calendar_entry)
            self._session.commit()  # obligar a que el evento que asigna los id se ejecute tantas veces como sea necesario


    def _create_account(self, group, amount, account_id):
        account = Cuentas()
        account.grupos = group
        account.id = account_id
        account.estatus = 'DESEMBOLSADA'
        account.monto = account.saldo = amount

        self._session.add(account)
        return account

        # try:
        #     self._session.add(account)
        #     self._session.commit()
            # self._generate_calendar(account, amount)
        #     return jsonify({'message': 'Cuenta creada exitosamente', 'code': 200})
        # except Exception as e:
        #     return jsonify({'code': 500, 'message': 'Ocurrió un error al crear la cuenta: %s' % str(e)})


    def get(self, account_id):

        if account_id is None:
            return jsonify(self.list_view())

        account_detail = self._session.query(self.model).get(account_id)

        return jsonify(self.detail(account_detail) if account_detail else {})


    def post(self, *arg, **kwargs):
        if request.form.get('generate_calendar'):
            loan_amount = request.form.get('loan_amount')

            if not loan_amount:
                return jsonify({'code': 400, 'message': 'Ingresa la cantidad del prestamo'})

            return jsonify({'code': 200, 'data': self._get_calendar_dates(loan_amount)})

        try:
            for key, field in [('group_id', 'ID Grupo'), ('account_id', 'Cuenta ID'), ('loan_amount', 'Monto')]:
                assert request.form.get(key), '<%s>' % field
        except AssertionError as e:
            return jsonify({'code': 400, 'message': 'Hace falta un campo requerido %s' % str(e)})

        account_id = request.form['account_id']

        if self._session.query(Cuentas).get(account_id):
            return jsonify({'code': 400, 'message': 'Ya existe la cuenta con id %s' % account_id})

        group = self._session.query(Grupos).get(request.form['group_id'])

        if not group:
            return jsonify({'code': 400, 'message': 'No existe el grupo con id %s' % request.form['group_id']})

        try:
            loan_amount = Decimal(request.form['loan_amount'])
        except InvalidOperation:
            return jsonify({'code': 400, 'message': 'Por favor ingresa una cantidad valida'})

        account = self._create_account(group, loan_amount, account_id)

        self._generate_calendar(account, loan_amount)

        return jsonify({'message': 'Cuenta creada exitosamente', 'code': 200})

