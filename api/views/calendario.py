from flask.views import MethodView
from flask import jsonify, request
from .serializer import serialize
from ..models import CalendarioPagos, Cuentas, session
from .mixins import ListViewMixin, GenerateTransactionsMixin
from decimal import Decimal, InvalidOperation
import datetime

class CalendarioView(MethodView, ListViewMixin, GenerateTransactionsMixin):
    _session = session
    model = CalendarioPagos


    def _update_payment_status(self, payment_calendar, amount):
        today = datetime.date.today()

        if amount < payment_calendar.monto:
            payment_calendar.estatus = 'PARCIAL'
        if today > payment_calendar.fecha_pago:
            payment_calendar.estatus = 'ATRASADO'
        if amount == payment_calendar.monto:
            payment_calendar.estatus = 'PAGADO'

        self._session.add(payment_calendar)


    def _get_payment_calendar(self, account_id, payment_num):
        return self._session.query(CalendarioPagos).filter_by(
                cuenta_id=account_id,
                num_pago=payment_num
        ).first()


    def get(self):
        return jsonify(self.list_view())

    def post(self):
        required_fields = [
            ('payment_id', 'ID pago'),
            ('payment_amount', 'Monto'),
            ('account_id', 'ID cuenta')
        ]

        try:
            for key, field in required_fields:
                assert request.form.get(key), '<%s>' % field
        except AssertionError as e:
            return jsonify({'status': 400, 'message': 'Por favor ingresa el campo %s ' % str(e)})

        account_id = request.form['account_id']
        payment_num = request.form['payment_id']

        to_pay = self._get_payment_calendar(account_id, payment_num)

        if not to_pay:
            return jsonify({'status': 400, 'message': 'No se encontró el registro a ser pagado'})

        try:
            amount = Decimal(request.form['payment_amount'])
            if amount > to_pay.monto:
                return jsonify({'status': 400, 'message': 'El pago no puede ser mayor al pactado %s' % request.form['payment_amount']})
        except InvalidOperation:
            return jsonify({'status': 400, 'message': 'Introduce una cantidad correcta'})


        self._update_payment_status(to_pay, amount)

        transaction_date = datetime.datetime.now()
        return self._create_transaction(to_pay.cuentas, amount, transaction_date)

