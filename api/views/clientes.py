from flask.views import MethodView
from flask import jsonify, request
from .serializer import serialize
from ..models import Clientes, session
from .mixins import ListViewMixin


class ClienteView(MethodView, ListViewMixin):
    _session = session
    model = Clientes

    def _create_client(self, client_id, name):
        client = Clientes(id=client_id, nombre=name)
        self._session.add(client)
        self._session.commit()

    def _is_duplicated(self, client_id, name):
        return self._session.query(Clientes).filter_by(id=client_id).count() >= 1

    def get(self):
        return jsonify(self.list_view())

    def post(self):

        try:
            for key, field in [('client_id', 'ID Cliente'), ('client_name', 'Nombre')]:
                assert request.form.get(key), '<%s>' % field
        except AssertionError as e:
            return jsonify({'code': 400, 'message': 'Por favor proporciona el campo %s' % str(e)})

        client_id = request.form['client_id']
        client_name = request.form['client_name']

        if self._is_duplicated(client_id, client_name):
            return jsonify({'code': 400, 'message': 'Ya existe un cliente con el ID proporcionado'})
        else:
            self._create_client(client_id, client_name)
            return jsonify({'code': 200, 'message': 'Cliente creado'})
