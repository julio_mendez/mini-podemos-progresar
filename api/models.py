from sqlalchemy import create_engine, event, func
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import sessionmaker

Base = automap_base()
engine = create_engine('mysql://eval:password@localhost/podemos_eval')

Base.prepare(engine, reflect=True)

NUM_PAGOS_X_PRESTAMOS = 4

Grupos = Base.classes.grupos
CalendarioPagos = Base.classes.calendariopagos
Clientes = Base.classes.clientes
Cuentas = Base.classes.cuentas
Transacciones = Base.classes.transacciones

Session = sessionmaker(bind=engine)
session = Session()

@event.listens_for(Transacciones, 'before_insert')
def set_transaction_id(mapper, connection, transaction):
    last_id = session.query(func.max(Transacciones.id)).scalar()
    transaction.id = last_id + 1

@event.listens_for(CalendarioPagos, 'before_insert')
def set_transaction_id(mapper, connection, calendar):
    last_id = session.query(func.max(CalendarioPagos.id)).scalar()
    calendar.id = last_id + 1
