
$('#createAccount').on('click', function () {
    let groupId = window.location.pathname.split('/')[2];  // se hace split a una cadena con forma /grupo/<id_grupo>
    let formData = new FormData();
    $('#error-message').remove()

    formData.append('group_id', groupId);
    formData.append('account_id', $('#accountId').val())
    formData.append('loan_amount', $('#loanAmount').val());

    fetch('http://localhost:7002/api/cuentas/', { method: 'POST', body: formData })
        .then(res => res.json())
        .then(function (response) {
            if (response.code == 200) {
                location.reload();
            }
            if (response.code == 400) {
                let alert = $('<div/>', { id: 'error-message', class: 'alert alert-warning' }).text(response.message);
                alert.appendTo($('.modal-body'));
            }
        });
});

$('#generateCalendar').on('click', function () {
    $('#payments-info').remove();
    $('#error-message').remove();
    let formData = new FormData();

    formData.append('loan_amount', $('#loanAmount').val());
    formData.append('generate_calendar', true);

    fetch('http://localhost:7002/api/cuentas/', { method: 'POST', body: formData })
        .then(res => res.json())
        .then(function (response) {
            if (response.code == 400) {
                var alert = $('<div/>', { id: 'error-message', class: 'alert alert-warning' }).text(response.message);
                alert.appendTo($('.modal-body'));
            }
            if (response.code == 200) {
                let calendarDiv = $('<div/>', { id: 'payments-info' });
                let orderList = $('<ol>');
                let paymentAmount = response.data.amount;
                let amountAsCurrency = '$' + (paymentAmount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                let amountHeader = $('<h5>').text('Cada uno de los pagos será por: ' + amountAsCurrency).append('<h5>Las fechas de pago son:</h5>');
                amountHeader.appendTo(calendarDiv);

                for (let i = 0; i < response.data.dates.length; i++) {
                    let date = response.data.dates[i];
                    let item = $('<li>').text(date);
                    item.appendTo(orderList);
                }
                orderList.appendTo(calendarDiv)
                calendarDiv.appendTo($('.modal-body'));
            }
        })
});