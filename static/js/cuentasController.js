
let paymentId = '';

// Evento que se ejecuta al abrir el modal, de aquí es donde tomamos num_pago
// para saber cual es la entrada del calendario que se quiere pagar
$('.trigger-transaction').on('click', function (event) {
    let row = $(event.currentTarget);
    paymentId = row.find("td:eq(1)").text();
});

$('#createPayment').on('click', function () {
    let paymentAmount = $('#paymentAmount').val();
    let accountId = window.location.pathname.split('/')[2];
    let formData = new FormData();

    formData.append('payment_id', paymentId);
    formData.append('payment_amount', paymentAmount);
    formData.append('account_id', accountId);

    fetch('http://localhost:7002/api/calendario/', { method: 'POST', body: formData })
        .then(res => res.json())
        .then(function (response) {
            if (response.code == 200) {
                location.reload();
            }
            if (response.code == 400) {
                let alert = $('<div>').addClass('alert alert-warning').text(response.message);
                alert.appendTo($('#calendar-payment-modal-body'));
            }
        })
});

$('#closeAccount').on('click', function () {
    let formData = new FormData();
    let accountId = window.location.pathname.split('/')[2];  // se hace split a una cadena con forma /cuenta/<id_cuenta>
    formData.append('account_id', accountId);
    formData.append('transaction_amount', $('#transactionAmount').val());
    formData.append('transaction_date', $('#transactionDate').val());

    fetch('http://localhost:7002/api/transacciones/', { method: 'POST', body: formData })
        .then(res => res.json())
        .then(function (response) {
            if (response.code == 200) {
                location.reload(true);
            }
            if (response.code == 400) {
                let alert = $('<div>').addClass('alert alert-warning').text(response.message);
                alert.appendTo($('#close-account-modal-body'));
            }
        })
});
