$('#createClient').on('click', function () {
    let formData = new FormData();
    formData.append('client_id', $('#clientId').val());
    formData.append('client_name', $('#clientName').val());
    $('#error-message').remove()

    fetch('http://localhost:7002/api/clientes/', { method: 'POST', body: formData })
        .then(res => res.json())
        .then(function (response) {
            if (response.code == 200) {
                location.reload()
            }
            if (response.code == 400) {
                let alert = $('<div/>', { id: 'error-message', class: 'alert alert-warning' }).text(response.message);
                alert.appendTo($('.modal-body'));
            }
        });
});